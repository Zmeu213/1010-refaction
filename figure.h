#ifndef FIGURE_H
#define FIGURE_H
#include <QVector>
struct Figure
{
    int height, width;
    int** figureMatrix;
    Figure(int h, int w, int** fM);
    Figure() {}
    virtual ~Figure() {
        for (int i = 0; i < height; ++i) {
            delete [] figureMatrix[i];
        }
        delete figureMatrix;
    }
    Figure* operator=(const Figure& f);
};

class FigureToBoardInterface : protected Figure {
public:
    virtual QVector< QVector < int > > getQVector();
    virtual Figure* getFigure();
};

class Angle : public FigureToBoardInterface {
public:
    Angle(bool right = true, bool up = true);
};

class Line : public FigureToBoardInterface {
private:
    void initAndFillArray(int h, int w);
public:
    Line(int lenght = 1, bool vertical = true);
};

class Square : public FigureToBoardInterface {
private:
    void initAndFillArray(int h, int w);
public:
    Square(bool small = true);
};

#endif // FIGURE_H
