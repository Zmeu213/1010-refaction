#include "figure.h"
#define angle_color 1
#define line_color 2
#define square_color 3
Figure::Figure(int h, int w, int **fM) {
    figureMatrix = new int*[h];
    height = h;
    width = w;
    for (int i = 0; i < h; ++i) {
        figureMatrix[i] = new int[w];
        for (int j = 0; j < w; ++j) {
           figureMatrix[i][j] = fM[i][j];
        }
    }
}

QVector<QVector<int> > FigureToBoardInterface::getQVector(){
    QVector<QVector<int> > result(height);
    for (int i = 0; i < height; ++i) {
        result[i].resize(width);
        for (int j = 0; j < width; ++j) {
            result[i][j] = figureMatrix[i][j];
        }
    }
    return result;
}

Figure* FigureToBoardInterface::getFigure() {
    Figure *tmp = new Figure(height, width, figureMatrix);
    return tmp;
}

Angle::Angle(bool right, bool up) {
    height = 3;
    width = 3;
    figureMatrix = new int* [height];
    for (int i = 0; i< height;++i) {
        figureMatrix[i] = new int [width];
        for (int j = 0; j<width; ++j) {
            figureMatrix[i][j] = 0;
        }
    }
    for (int i = 0; i < height; ++i) {
        if ( right ) figureMatrix[i][0] = angle_color;
        else figureMatrix[i][2] = angle_color;
    }
    for (int i = 0; i < width; ++i) {
        if ( up ) figureMatrix[2][i] = angle_color;
        else figureMatrix[0][i] = angle_color;
    }

}

void Line::initAndFillArray(int h, int w) {
    figureMatrix = new int*[h];
    if (h == 1) {
        figureMatrix[0] = new int[w];
        for (int i = 0; i < w; ++i) {
            figureMatrix[0][i] = line_color;
        }
    } else {
        for (int i = 0; i < h; ++i) {
            figureMatrix[i] = new int(line_color);
        }
    }
}

Line::Line(int lenght, bool vertical) {
    if ( vertical ) {
        width = 1;
        height = lenght;
    } else {
        width = lenght;
        height = 1;
    }
    initAndFillArray(height, width);
}

void Square::initAndFillArray(int h, int w) {
    figureMatrix = new int*[h];
    for (int i = 0; i < h; ++i) {
        figureMatrix[i] = new int[w];
        for (int j = 0; j < w; ++j) {
            figureMatrix[i][j] = square_color;
        }
    }
}

Square::Square(bool small) {
    if ( small ) {
        height = 2;
        width = 2;
    } else {
        height = 3;
        width = 3;
    }
    initAndFillArray(height, width);
}

Figure* Figure::operator=(const Figure &f) {
    height = f.height;
    width = f.width;
    figureMatrix = new int*[height];
    for (int i = 0; i < height; ++i) {
        figureMatrix[i] = new int[width];
        for (int j = 0; j < width; ++j) {
           figureMatrix[i][j] = f.figureMatrix[i][j];
        }
    }
}
