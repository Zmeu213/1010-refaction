#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sForm = new ScoreForm();
    scn = new GameScene();
    this->ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    setFixedSize(680,700);
    ui->graphicsView->setScene(scn);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_Game_2_triggered()
{
    QMessageBox qmsgBox;
    int tmp = scn->newGame();
    qmsgBox.setText("Game Over");
    qmsgBox.setInformativeText("Your socre "+QString::number(tmp)+"\n Save?");
    qmsgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        qmsgBox.setDefaultButton(QMessageBox::No);
        qmsgBox.setButtonText(QMessageBox::Yes, "Да");
        qmsgBox.setButtonText(QMessageBox::No, "Нет");

    if (qmsgBox.exec() == QMessageBox::Yes) {
        QSqlQuery query;
        QString str = "INSERT INTO score (date, score) VALUES ('%1', '%2')";
        str = str.arg(QDateTime::currentDateTime().toString()).arg(tmp);
        query.exec(str);
        qDebug() << query.lastError();
    }

}

void MainWindow::on_actionTop_Results_triggered(){
    sForm->show();
}
