#ifndef SCOREFORM_H
#define SCOREFORM_H
#include <QWidget>
#include <QSqlTableModel>
namespace Ui {
class ScoreForm;
}

class ScoreForm : public QWidget
{
    Q_OBJECT
protected:
    void showEvent(QShowEvent *);
public:
    explicit ScoreForm(QWidget *parent = 0);
    ~ScoreForm();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ScoreForm *ui;
};

#endif // SCOREFORM_H
