#include "printmyfigure.h"

PrintMyFigure::PrintMyFigure(int id, int xshift,int yshift, QVector<QVector < int> > figure) {
    this->id = id;
    this->yshift = yshift;
    this->xshift = xshift;
    this->figure = figure;
    height = figure.size();
    if (height) width = figure[0].size();
}

QRectF PrintMyFigure::boundingRect() const {
    return QRectF(xshift, yshift, width*34, height*34);
}

void PrintMyFigure::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    for (int i = 0; i < height;++i) {
        for (int j = 0; j < width; ++j) {
            if (figure[i][j]) {
                QPen pen;
                pen.setWidth(0);
                painter->setBrush(getBrush(figure[i][j]));
                painter->drawRoundedRect(xshift + 34*j, yshift+34*i, 30, 30, 10, 10);
            }
        }
    }
}

QPoint PrintMyFigure::getPos() {
    return QPoint(this->pos().x() + xshift, this->pos().y() + yshift);
}

int PrintMyFigure::getId() {
    return id;
}

QBrush PrintMyFigure::getBrush(int id) {
    switch (id) {
    case 1: return(QBrush(Qt::blue));
    case 2: return(QBrush(Qt::green));
    case 3: return(QBrush(Qt::red));
    }
}

void PrintMyFigure::returnBack() {
    this->setPos(0,0);
}

void PrintMyFigure::mousePressEvent(QGraphicsSceneMouseEvent *e) {
}


void PrintMyFigure::mouseReleaseEvent(QGraphicsSceneMouseEvent *e) {
    returnBack();
    QGraphicsItem::mousePressEvent(e);
}
