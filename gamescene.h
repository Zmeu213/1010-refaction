#ifndef GAMESCENE_H
#define GAMESCENE_H
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QBrush>
#include <QPainter>

#include "printmyfigure.h"
#include "board.h"

class GameScene : public QGraphicsScene
{
private:
    Board gameBoard;
protected:
    QBrush getBrush(int id);
    void setFigures();
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void printBoard();
public:
    GameScene();
    int newGame() {
        int tmp;
        tmp = gameBoard.getScore();
        gameBoard.newGame();
        printBoard();
        return tmp;
    }
};


class MyRoundRect : public QGraphicsItem
{
private:
    int x, y;
    int h, w;
    QBrush b;
public:
    MyRoundRect(int X, int Y, int H, int W, QBrush B) {
        x = X;
        y = Y;
        h = H;
        w = W;
        b = B;
    }
    QRectF boundingRect() const {
        return QRectF(x, y, h, w);
    }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
        painter->setBrush(b);
        QPen pen;
        pen.setWidth(0);
        painter->setPen(pen);
        painter->drawRoundedRect(x,y,h,w, 10, 10);
    }
};
#endif // GAMESCENE_H
