#include "gamescene.h"
GameScene::GameScene() {
    gameBoard.newGame();
    printBoard();
    setSceneRect(0,0,400,600);
}


QBrush GameScene::getBrush(int id) {
    switch (id) {
    case 0: return(QBrush(Qt::lightGray));
    case 1: return(QBrush(Qt::blue));
    case 2: return(QBrush(Qt::green));
    case 3: return(QBrush(Qt::red));
    }
}

void GameScene::setFigures() {
    QVector<QVector<QVector< int > > > figuresVector = gameBoard.getQFigures();
    int prevW = 0;
    for (int i = 0; i < figuresVector.size(); ++i ){
        if (i) {
            if (!figuresVector[i-1].isEmpty()) {
                prevW += figuresVector[i-1][0].size();
            }
        }
        PrintMyFigure *itm = new PrintMyFigure(i, 40*prevW+i*30, 425, figuresVector[i]);

        itm->setFlag(QGraphicsItem::ItemIsMovable);
        addItem(itm);
    }
}

void GameScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    PrintMyFigure *itm = dynamic_cast<PrintMyFigure*> (itemAt(event->scenePos().x(), event->scenePos().y(), QTransform()));
    if(itm) {
        if (gameBoard.putFigureOnBoard(itm->getPos().x() / 40, itm->getPos().y() / 40, itm->getId()))
        this->removeItem(itm);
        printBoard();
    }
    QGraphicsScene::mouseReleaseEvent(event);
}

void GameScene::printBoard() {
    clear();
    QString str = "Your score: %1";
    str = str.arg(gameBoard.getScore());
    addText(str);
    QVector <QVector<int> > board = gameBoard.getBoard();
    for (int i = 0; i < board.size(); ++i ) {
        for (int j = 0; j < board[0].size(); ++j) {
            MyRoundRect *itm = new MyRoundRect(40*j , 40*i+20, 35,35, getBrush(board[i][j]));
            addItem(itm);
        }
    }
    setFigures();
}

